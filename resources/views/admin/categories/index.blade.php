@extends('admin.app')

@section('content')

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Category Page</h3>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Categories</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-8">
                             <table class="table table hover">
                                 <thead>
                                    <th> Category Name </th>
                                 <th> Editing </th>
                                 <th> Deleting</th>
                                 </thead>
                                 <tbody>
                                       @foreach($categories as $category)
                             <tr>
                                 <td> {{ $category->name }}</td>
                                 <td>
                                     <a href="{{route('category.edit', ['id' => $category->id])}}" class="btn btn-xs btn-info">
                                         <i class="fa fa-edit" aria-hidden="true"></i>
                                     </a>
                                 </td>
                                 <td>
                                     <a href="{{route('category.delete', ['id' => $category->id])}}" class="btn btn-xs btn-danger">
                                         <i class="fa fa-trash" aria-hidden="true"></i>
                                     </a>
                                 </td>
                             </tr>
                                           @endforeach
                                 </tbody>
                             </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->

@endsection