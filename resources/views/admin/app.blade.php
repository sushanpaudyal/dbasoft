<!doctype html>
<html lang="en">
<head>
  @include('admin.layouts.includes.head')
</head>
<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="{{route('home')}}" class="site_title"><i class="fa fa-paw"></i> <span>DBASoftTech</span></a>
                </div>

                <div class="clearfix"></div>

                <!-- menu profile quick info -->
                <div class="profile clearfix">
                    {{--<div class="profile_pic">--}}
                        {{--<img src="{{asset('asset/admin/images/img.jpg')}}" alt="..." class="img-circle profile_img">--}}
                    {{--</div>--}}
                    <div class="profile_info">
                        <span>Welcome Back</span>
                        <h2></h2>
                    </div>
                </div>
                <!-- /menu profile quick info -->

                <br />

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        <h3>General</h3>
                        <ul class="nav side-menu">
                            <li><a><i class="fa fa-edit"></i> Posts <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{route('posts')}}">All Posts</a></li>
                                    <li><a href="{{route('post.create')}}">Create Post</a></li>
                                    <li><a href="{{route('post.trashed')}}">Trashed Post</a></li>
                                </ul>
                            </li>

                            <li><a><i class="fa fa-filter"></i> Category <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{route('categories')}}">All Categories</a></li>
                                    <li><a href="{{route('category.create')}}">Create Category</a></li>
                                </ul>
                            </li>

                            <li><a><i class="fa fa-bookmark-o"></i> Tags <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{route('tags')}}">All Tags</a></li>
                                    <li><a href="{{route('tag.create')}}">Create Tags</a></li>

                                </ul>
                            </li>
                            <li><a><i class="fa fa-users"></i> Users <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{route('users')}}">Users</a></li>
                                    <li><a href="{{route('user.create')}}">New User</a></li>

                                </ul>
                            </li>
                            {{--@if(Auth::user())--}}
                                <li><a><i class="fa fa-user"></i> My Profile <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">
                                        <li><a href="{{route('user.profile')}}">My Profile </a></li>
                                    </ul>
                                </li>
                                {{--@endif--}}

                        </ul>

                    </div>

                </div>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->
                <div class="sidebar-footer hidden-small">
                    <a data-toggle="tooltip" data-placement="top" title="Home" href="{{route('home')}}">
                        <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Visit Main Page" href="{{route('dba')}}" target="_blank">
                        <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Profile" href="{{route('user.profile')}}">
                        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Logout" href="{{route('logout')}}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </div>
                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <nav>
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    {{--<ul class="nav navbar-nav navbar-right">--}}
                        {{--<li class="">--}}
                            {{--<a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">--}}
                                {{--<img src="{{asset('asset/admin/images/img.jpg')}}" alt=""> --}}
                                {{--<span class=" fa fa-angle-down"></span>--}}
                            {{--</a>--}}
                            {{--<ul class="dropdown-menu dropdown-usermenu pull-right">--}}
                                {{--<li><a href=""> Profile</a></li>--}}
                                {{--<li>--}}
                                    {{--<a href="javascript:;">--}}
                                        {{--<span class="badge bg-red pull-right">50%</span>--}}
                                        {{--<span>Settings</span>--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                                {{--<li><a href="javascript:;">Help</a></li>--}}
                                {{--<li><a href="login.html"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>--}}
                                {{--<li><a href="{{route('user.profile')}}"><i class="fa fa-sign-out pull-right"></i> Profile </a></li>--}}
                            {{--</ul>--}}
                        {{--</li>--}}


                    {{--</ul>--}}
                </nav>
            </div>
        </div>
        <!-- /top navigation -->

       @yield('content')


    <!-- footer content -->
        <footer>
            <div class="pull-right">
                 Admin Panel <a href="https://colorlib.com"> DBASoftTech </a>
            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>

<!-- jQuery -->
<script src="{{asset('asset/admin/vendors/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap -->
<script src="{{asset('asset/admin/vendors/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('asset/admin/vendors/fastclick/lib/fastclick.js')}}"></script>
<!-- NProgress -->
<script src="{{asset('asset/admin/vendors/nprogress/nprogress.js')}}"></script>
<!-- Chart.js -->
<script src="{{asset('asset/admin/vendors/Chart.js/dist/Chart.min.js')}}"></script>
<!-- gauge.js -->
<script src="{{asset('asset/admin/vendors/gauge.js/dist/gauge.min.js')}}"></script>
<!-- bootstrap-progressbar -->
<script src="{{asset('asset/admin/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js')}}"></script>
<!-- iCheck -->
<script src="{{asset('asset/admin/vendors/iCheck/icheck.min.js')}}"></script>
<!-- Skycons -->
<script src="{{asset('asset/admin/vendors/skycons/skycons.js')}}"></script>
<!-- Flot -->
<script src="{{asset('asset/admin/vendors/Flot/jquery.flot.js')}}"></script>
<script src="{{asset('asset/admin/vendors/Flot/jquery.flot.pie.js')}}"></script>
<script src="{{asset('asset/admin/vendors/Flot/jquery.flot.time.js')}}"></script>
<script src="{{asset('asset/admin/vendors/Flot/jquery.flot.stack.js')}}"></script>
<script src="{{asset('asset/admin/vendors/Flot/jquery.flot.resize.js')}}"></script>
<!-- Flot plugins -->
<script src="{{asset('asset/admin/vendors/flot.orderbars/js/jquery.flot.orderBars.js')}}"></script>
<script src="{{asset('asset/admin/vendors/flot-spline/js/jquery.flot.spline.min.js')}}"></script>
<script src="{{asset('asset/admin/vendors/flot.curvedlines/curvedLines.js')}}"></script>
<!-- DateJS -->
<script src="{{asset('asset/admin/vendors/DateJS/build/date.js')}}"></script>
<!-- JQVMap -->
<script src="{{asset('asset/admin/vendors/jqvmap/dist/jquery.vmap.js')}}"></script>
<script src="{{asset('asset/admin/vendors/jqvmap/dist/maps/jquery.vmap.world.js')}}"></script>
<script src="{{asset('asset/admin/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js')}}"></script>
<!-- bootstrap-daterangepicker -->
<script src="{{asset('asset/admin/vendors/moment/min/moment.min.js')}}"></script>
<script src="{{asset('asset/admin/vendors/bootstrap-daterangepicker/daterangepicker.js')}}"></script>

<!-- Custom Theme Scripts -->
<script src="{{asset('asset/admin/build/js/custom.min.js')}}"></script>
<script src="{{asset('js/toastr.min.js')}}"></script>
<script>
    @if(Session::has('success'))
        toastr.success('{{ Session::get('success') }}')
        @endif
</script>
<script>
    @if(Session::has('info'))
        toastr.info('{{Session::get('info')}}')
        @endif
</script>

@yield('scripts')

</body>
</html>