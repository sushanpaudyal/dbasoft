@extends('admin.app')

@section('content')



    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Post Page</h3>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
                @if(count($errors)> 0)
                    <ul class="list-group">
                        @foreach($errors->all() as $error)
                            <li class="list-group-item text-danger">
                                {{$error}}
                            </li>
                        @endforeach
                    </ul>
                @endif
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Edit Your Post</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>

                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-8">
                                        <form action="{{route('posts.update', ['id' => $post->id])}}" method="post" enctype="multipart/form-data">
                                            {{csrf_field()}}
                                            <div class="form-group">
                                                <label for="title">Post Title</label>
                                                <input type="text" class="form-control" name="title" value="{{$post->title}}">
                                            </div>
                                            <div class="form-group">
                                                <label for="featured">Featured Image</label>
                                                <input type="file" class="form-control" name="featured">
                                            </div>
                                            <div class="form-group">
                                                <label for="category">Select a Category</label>
                                                <select name="category_id" id="category" class="form-control">

                                                    @foreach($categories as $category)
                                                        <option value="{{$category->id}}"
                                                        @if($post->category->id == $category->id)
                                                            selected
                                                        @endif> {{$category->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="tags">Select Tags</label>
                                                @foreach($tags as $tag)
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" value="{{$tag->id}}" name="tags[]"
                                                            @foreach($post->tags as $t)
                                                                @if($tag->id == $t->id)
                                                                    checked
                                                            @endif
                                                            @endforeach>
                                                            {{$tag->tag}}
                                                        </label>
                                                    </div>
                                                @endforeach
                                            </div>
                                            <div class="form-group">
                                                <label for="content">Post Content</label>
                                                <textarea name="content" id="content" cols="8" rows="15" class="form-control">{{$post->content}}</textarea>
                                            </div>

                                            <div class="form-group">
                                                <div class="text-center">
                                                    <button class="btn btn-success" type="submit">Update Post</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->

@endsection

@section('styles')
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.7/summernote.css" rel="stylesheet">
@endsection

@section('scripts')
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.7/summernote.js"></script>
    <script>
        $(document).ready(function() {
            $('#content').summernote();
        });
    </script>
@endsection