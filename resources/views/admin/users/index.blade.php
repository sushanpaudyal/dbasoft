@extends('admin.app')

@section('content')

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Users Page</h3>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Users</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-8">
                                        <table class="table table hover">
                                            <thead>
                                            <th>Image</th>
                                            <th> User Name </th>
                                            <th>Email</th>
                                            <th> Permissions </th>
                                            <th> Delete</th>
                                            </thead>
                                            <tbody>
                                            @if($users->count() > 0)
                                            @foreach($users as $user)
                                                <tr>
                                                    <td><img src="{{asset($user->profile->avatar)}}" width="100px" height="50px" style="border-radius: 50%;"></td>
                                                    <td>{{$user->name}}</td>
                                                    <td>{{$user->email}}</td>
                                                    <td>
                                                        @if($user->admin)
                                                            <a href="{{route('user.notadmin', ['id' => $user->id])}}" class="btn btn-xs btn-success"> Remove Permission</a>
                                                            @else
                                                            <a href="{{route('user.admin', ['id' => $user->id])}}" class="btn btn-xs btn-info"> Make Admin </a>
                                                            @endif
                                                    </td>
                                                    <td>
                                                        @if(Auth::id() !== $user->id)
                                                            <a href="{{route('user.delete', ['id' => $user->id])}}" class="btn btn-xs btn-danger"> Delete</a>
                                                            @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                                @else
                                            <tr>
                                                <th colspan="5" class="text-center"> No Users Yet </th>
                                            </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->

@endsection