@extends('admin.app')

@section('content')

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Profile Page</h3>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Update Your Profile</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-8">
                                        <form action="{{route('user.profile.update')}}" method="post" enctype="multipart/form-data">
                                            {{csrf_field()}}

                                            <div class="form-group">
                                                <label for="name">User Name</label>
                                                <input type="text" class="form-control" name="name" value="{{$user->name}}">
                                            </div>
                                            <div class="form-group">
                                                <label for="email">Email Address</label>
                                                <input type="email" class="form-control" name="email" value="{{$user->email}}" disabled="disabled">
                                            </div>
                                            <div class="form-group">
                                                <label for="password">New Password</label>
                                                <input type="password" class="form-control" name="password">
                                            </div>
                                            <div class="form-group">
                                                <label for="avatar">Upload Image</label>
                                                <input type="file" name="avatar" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label for="facebook">Facebook Profile</label>
                                                <input type="text" class="form-control" name="facebook" value="{{$user->profile->facebook}}" }>
                                            </div>
                                            <div class="form-group">
                                                <label for="linkedin">Linkedin Profile</label>
                                                <input type="text" class="form-control" name="linkedin" value="{{$user->profile->linkedin}}">
                                            </div>
                                            <div class="form-group">
                                                <label for="about">About You</label>
                                                <textarea name="about" id="about" cols="30" rows="10" class="form-control">{{$user->profile->about}}</textarea>
                                            </div>
                                            <div class="form-group">
                                                <div class="text-center">
                                                    <button class="btn btn-success" type="submit">Update Profile</button>
                                                </div>

                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->

@endsection

@section('styles')
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.7/summernote.css" rel="stylesheet">
@endsection

@section('scripts')
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.7/summernote.js"></script>
    <script>
        $(document).ready(function() {
            $('#about').summernote();
        });
    </script>
@endsection