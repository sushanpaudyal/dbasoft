<footer class="footer footer-black footer-big">
    <div class="container">

        <div class="content">
            <div class="row">
                <div class="col-md-4">
                    <h5>About Us</h5>
                    <p>DBASoftTech is an IT company empowering and trying to empower every sector with new tools and
                        technologies in information technology. We are a group of IT Professionals driven towards
                        implementing the best of technology and providing solution for various disciplines with the help
                        of information technology.Our dedicated consistently delivers superior quality products and
                        services.</p>
                </div>

                <div class="col-md-4">
                    <h5>Social Feed</h5>
                    <div class="social-feed">
                        <div class="feed-line">
                            <i class="fa fa-twitter"></i>
                            <p>How to handle ethical disagreements with your clients.</p>
                        </div>
                        <div class="feed-line">
                            <i class="fa fa-twitter"></i>
                            <p>The tangible benefits of designing at 1x pixel density.</p>
                        </div>
                        <div class="feed-line">
                            <i class="fa fa-facebook-square"></i>
                            <p>A collection of 25 stunning sites that you can use for inspiration.</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <h5>Recent Hub</h5>

                    <ul class="links-vertical">
                        <li><a href="#"> Curabitur nec justo eu eros maximus.</a></li>
                        <li><a href="#"> Curabitur nec justo eu eros maximus.</a></li>
                        <li><a href="#"> Curabitur nec justo eu eros maximus.</a></li>
                        <li><a href="#"> Curabitur nec justo eu eros maximus.</a></li>
                        <li><a href="#"> Curabitur nec justo eu eros maximus.</a></li>
                    </ul>

                </div>
            </div>
        </div>


        <hr>
        <ul class="pull-left">
            <li>
                <a href="#pablo">
                    Sitemap
                </a>
            </li>
            <li>
                <a href="#pablo">
                    Privacy Policy
                </a>
            </li>
            <li>
                <a href="#pablo">
                    Terms and Conditions
                </a>
            </li>
        </ul>

        <div class="copyright pull-right">
            Copyright ©
            <script>document.write(new Date().getFullYear())</script>
            DBASoftTech All Rights Reserved.
        </div>
    </div>
</footer>
<script type="text/javascript" src="{{asset('asset/js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('asset/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('asset/js/slick.min.js')}}"></script>
<script type="text/javascript" src="{{asset('asset/js/material.min.js')}}"></script>
<script type="text/javascript" src="{{asset('asset/js/bootstrap-selectpicker.js')}}"></script>
<script type="text/javascript" src="{{asset('asset/js/bootstrap-tagsinput.js')}}"></script>
<script type="text/javascript" src="{{asset('asset/js/jasny-bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('asset/js/atv-img-animation.js')}}"></script>
<script type="text/javascript" src="{{asset('asset/js/material-kit.min.js?v=1.1.0')}}"></script>
<script type="text/javascript" src="{{asset('asset/js/modernizr.js')}}"></script>
<script type="text/javascript"
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCySZBaf8r-9KgbnCgxEkm9kvgk1KcTBkI"></script>
<script type="text/javascript" src="{{asset('asset/js/core.js')}}"></script>