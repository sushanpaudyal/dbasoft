<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="google-site-verification" content="2nL0xoxXo8D0uz9P1zrA_WmHNhPo590Qzz97ui7Y6JQ" />

<title>DBASoftTech</title>
<meta name="keywords" content="website development, website development company in nepal, software design, website design, software development company in nepal, software development, IT company in nepal, IT company,responsive web design, responsive layouts, digital marketing agency, SEO(search engine optimization), social media marketing, facebook marketing for small business, twitter marketing, youtube marketing, google analytic, google adword, email marketing, online advertising, latest web technology, live support software, high security,
 domain registration, web hosting, logo designs, Graphic Designs, ecommerce solutions"/>
<meta name="description" content="DBASofttech is an IT company empowering the development of responsive websites and software to promote your business"/>
<meta name="robots" content="follow,index" />

<link rel="canonical" href="http://dbasofttech.com/" />

<!-- Fonts -->
<link rel="stylesheet" type="text/css" href="{{asset('asset/css/app.css')}}">
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css"/>
<!-- CSS Files -->
<link href="{{asset('asset/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('asset/css/material-kit.min.css?v=1.1.0')}}" rel="stylesheet" type="text/css">
<link href="{{asset('asset/css/slick.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('asset/css/demo.css')}}" rel="stylesheet" type="text/css">