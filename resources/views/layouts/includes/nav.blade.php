<div class="header-2">
    <nav class="navbar navbar-default navbar-fixed-top navbar-color-on-scroll navbar-transparent" id="sectionsNav">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">
                    <div class="logo"></div>
                </a>
            </div>

            <div class="navbar-collapse navi-right">
                <div class="pull-right">
                    <ul class="nav navbar-nav ">
                        <li><a href="">Home</a></li>
                        <li><a href="#team">Team</a></li>
                        <li><a href="#projects-2">Portfolio</a></li>
                        <li><a href="#contactsection">Contact Us</a></li>
                        <li><a href="/blog" target="_blank">Blog</a></li>
                    </ul>
                </div>

            </div>
        </div>
    </nav>
</div>
<div class="page-header header-filter"
     style="background-image: url(asset/images/office2.jpg);">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-center">
                <h1 class="title">
                    You should work with us!
                </h1>
                <h4>Our motto is to address our customer to the top level.We create platform to our customer to
                    generate their own customer
                </h4>
            </div>
            <div class="col-md-10 col-md-offset-1">

                <div class="text-center btn-class-custom">
                    <button id="serviceswe" class="btn btn-primary btn-round">
                        <i class="material-icons">play_for_work</i> Learn More
                        <div class="ripple-container"></div>
                    </button>
                </div>

            </div>
        </div>
    </div>
</div>