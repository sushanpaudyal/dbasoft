@extends('layouts.app')

@section('content')
    <div class="section text-center section-landing corecss">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <h2 class="title">Who we are</h2>
                    <h5 class="description">
                        DBASoftTech is an IT company empowering and trying to empower every sector with new tools and
                        technologies in information technology. We are a group of IT Professionals driven towards
                        implementing the best of technology and providing solution for various disciplines with the help
                        of information technology.Our dedicated consistently delivers superior quality products and
                        services.</h5>
                </div>
            </div>
        </div>
    </div>

    <div id="servicesour" class="features-5 corecss-services">

        <div class="col-md-8 col-md-offset-2 text-center">
            <h2 class="title">Services We Provide</h2>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="info">
                        <div class="icon">
                            <i class="material-icons">code</i>
                        </div>
                        <h4 class="info-title">Web/Software Development</h4>
                        <p>We serve all kinds of website and web application development services that meets your needs
                            for online presence and competition. </p>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="info">
                        <div class="icon">
                            <i class="material-icons">format_paint</i>
                        </div>
                        <h4 class="info-title">Web/Software Design</h4>
                        <p>User Experience is the most special requirement for you to meet in your website. We have
                            mastered the science of User Xperience. </p>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="info">
                        <div class="icon">
                            <i class="material-icons">devices</i>
                        </div>
                        <h4 class="info-title">Responsive layouts</h4>
                        <p>Responsive web design (RWD) is an approach to web design aimed at crafting sites to provide
                            an optimal viewing experience. </p>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-sm-4">
                    <div class="info">
                        <div class="icon">
                            <i class="material-icons">search</i>
                        </div>
                        <h4 class="info-title">(SEO) Search Engine Optimization</h4>
                        <p>Strategies, techniques and tactics used to increase the amount of
                            visitors to a website by obtaining a high-ranking placement in the search results page of a
                            search engine. </p>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="info">
                        <div class="icon">
                            <i class="material-icons">shopping_cart</i>
                        </div>
                        <h4 class="info-title">E-Commerce Solution</h4>
                        <p>We count on experienced e-commerce programmers using Woo-Commerce, Big-Commerce and
                            Shopify. </p>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="info">
                        <div class="icon">
                            <i class="material-icons">speaker_notes</i>
                        </div>
                        <h4 class="info-title">Support</h4>
                        <p>We offer maintenance packages at reduced rates, based on how often you anticipate making
                            changes or additions to your site. </p>
                    </div>
                </div>
            </div>

        </div>
    </div>



    <div class="call-to-action">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <h3 class="title">Build professional website / Software according to your need.</h3>
                </div>
                <div class="col-md-3">
                    <div class="card card-plain card-form-horizontal">
                        <div class="card-content">
                            <button id="contact" type="button" class="btn btn-rose btn-round btn-block btn-lg">Know
                                us
                            </button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="cd-section" id="features">
        <div class="container">
            <!--     *********     FEATURES 1      *********      -->
            <div class="features-1">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <h2 class="title">
                            Why our product is the best
                        </h2>
                        <h5 class="description">
                            Our priority is solving problems for you,not giving you more to worry about. We also know
                            that forming strategic partnerships with our clients is a key to success.
                        </h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="info">
                            <div class="icon icon-info">
                                <i class="material-icons">trending_up</i>
                            </div>
                            <h4 class="info-title">
                                Latest Technology
                            </h4>




                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="info">
                            <div class="icon icon-success">
                                <i class="material-icons">
                                    security
                                </i>
                            </div>
                            <h4 class="info-title">
                                High Security
                            </h4>



                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="info">
                            <div class="icon icon-danger">
                                <i class="material-icons">supervisor_account</i>
                            </div>
                            <h4 class="info-title">
                                Live support
                            </h4>



                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end section -->
    <div class="projects-2" id="projects-2">

        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <h2 class="title">Our work</h2>
                    <h5 class="description">We have been working in this field since 7 years.Do check out our portfolio..</h5>
                    <div class="section-space"></div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 col-lg-4">
                    <div class="card card-blog card-atv">
                        <div class="card-image">
                            <a href="#">
                                <div class="atvImg" id="atvImg__1"
                                     style="height: 199.531px; transform: perspective(990px);">
                                    <div class="atvImg-container">
                                        <div class="atvImg-shadow"></div>
                                        <div class="atvImg-layers">
                                            <div class="atvImg">
                                                <div class="atvImg-layer"
                                                     data-img="{{asset('asset/images/ktm.png')}}"></div>
                                            </div>
                                        </div>
                                        <div class="atvImg-shine"></div>
                                    </div>
                                </div>
                            </a>
                            <div class="colored-shadow"
                                 style="background-image: url('asset/images/ktm.png'); opacity: 1;"></div>
                            <div class="ripple-container"></div>
                        </div>

                        <div class="card-content">
                            <h4 class="card-title">
                                <a href="http://ktmimmigration.com.np/" target="_blank">Ktm Immigration</a>
                            </h4>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-4">
                    <div class="card card-blog card-atv">
                        <div class="card-image">
                            <a href="#">
                                <div class="atvImg" id="atvImg__1"
                                     style="height: 199.531px; transform: perspective(990px);">
                                    <div class="atvImg-container">
                                        <div class="atvImg-shadow"></div>
                                        <div class="atvImg-layers">
                                            <div class="atvImg">
                                                <div class="atvImg-layer"
                                                     data-img="{{asset('asset/images/sumeru1.png')}}"></div>
                                            </div>
                                        </div>
                                        <div class="atvImg-shine"></div>
                                    </div>
                                </div>
                            </a>
                            <div class="colored-shadow"
                                 style="background-image: url('asset/images/sumeru1.png'); opacity: 1;"></div>
                            <div class="ripple-container"></div>
                        </div>

                        <div class="card-content">
                            <h4 class="card-title">
                                <a href="http://sumerusecurities.com/" target="_blank">SumeruSecurities</a>
                            </h4>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-4">
                    <div class="card card-blog card-atv">
                        <div class="card-image">
                            <a href="#">
                                <div class="atvImg" id="atvImg__1"
                                     style="height: 199.531px; transform: perspective(990px);">
                                    <div class="atvImg-container">
                                        <div class="atvImg-shadow"></div>
                                        <div class="atvImg-layers">
                                            <div class="atvImg">
                                                <div class="atvImg-layer"
                                                     data-img="{{asset('asset/images/jaynepal.png')}}"></div>
                                            </div>
                                        </div>
                                        <div class="atvImg-shine"></div>
                                    </div>
                                </div>
                            </a>
                            <div class="colored-shadow"
                                 style="background-image: url('asset/images/ktm.png'); opacity: 1;"></div>
                            <div class="ripple-container"></div>
                        </div>

                        <div class="card-content">
                            <h4 class="card-title">
                                <a href="http://jaynepalsocialyouth.org/" target="_blank">jaynepalsocialyouth.org</a>
                            </h4>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-4">
                    <div class="card card-blog card-atv">
                        <div class="card-image">
                            <a href="http://www.blackstonevalleygroup.com/">
                                <div class="atvImg" id="atvImg__1"
                                     style="height: 199.531px; transform: perspective(990px);">
                                    <div class="atvImg-container">
                                        <div class="atvImg-shadow"></div>
                                        <div class="atvImg-layers">
                                            <div class="atvImg">
                                                <div class="atvImg-layer"
                                                     data-img="{{asset('asset/images/blackstone.png')}}"></div>
                                            </div>
                                        </div>
                                        <div class="atvImg-shine"></div>
                                    </div>
                                </div>
                            </a>
                            <div class="colored-shadow"
                                 style="background-image: url('asset/images/blackstone.png'); opacity: 1;"></div>
                            <div class="ripple-container"></div>
                        </div>

                        <div class="card-content">
                            <h4 class="card-title">
                                <a href="http://www.blackstonevalleygroup.com/" target="_blank">Backstonevalleygroup</a>
                            </h4>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-4">
                    <div class="card card-blog card-atv">
                        <div class="card-image">
                            <a href="#">
                                <div class="atvImg" id="atvImg__1"
                                     style="height: 199.531px; transform: perspective(990px);">
                                    <div class="atvImg-container">
                                        <div class="atvImg-shadow"></div>
                                        <div class="atvImg-layers">
                                            <div class="atvImg">
                                                <div class="atvImg-layer"
                                                     data-img="{{asset('asset/images/damasco.png')}}"></div>
                                            </div>
                                        </div>
                                        <div class="atvImg-shine"></div>
                                    </div>
                                </div>
                            </a>
                            <div class="colored-shadow"
                                 style="background-image: url('asset/images/damasco.png'); opacity: 1;"></div>
                            <div class="ripple-container"></div>
                        </div>

                        <div class="card-content">
                            <h4 class="card-title">
                                <a href="http://www.damascoqatar.com/" target="_blank">Damasco Qatar</a>
                            </h4>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-4">
                    <div class="card card-blog card-atv">
                        <div class="card-image">
                            <a href="#">
                                <div class="atvImg" id="atvImg__1"
                                     style="height: 199.531px; transform: perspective(990px);">
                                    <div class="atvImg-container">
                                        <div class="atvImg-shadow"></div>
                                        <div class="atvImg-layers">
                                            <div class="atvImg">
                                                <div class="atvImg-layer"
                                                     data-img="{{asset('asset/images/dhara.png')}}"></div>
                                            </div>
                                        </div>
                                        <div class="atvImg-shine"></div>
                                    </div>
                                </div>
                            </a>
                            <div class="colored-shadow"
                                 style="background-image: url('asset/images/dhara.png'); opacity: 1;"></div>
                            <div class="ripple-container"></div>
                        </div>

                        <div class="card-content">
                            <h4 class="card-title">
                                <a href="http://www.dichm.com/" target="_blank">DICHM</a>
                            </h4>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>




    <div class="team-4 section-image" style="background-image: url(asset/images/bg7.jpg)" id="team">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <h2 class="title">
                        Meet our awesome team
                    </h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 card-5">
                    <div class="card card-profile">
                        <div class="card-avatar">
                            <img class="img" src="{{asset('asset/images/binay.jpg')}}">
                        </div>
                        <div class="card-content">
                            <h4 class="card-title">
                                Binaya Kharel
                            </h4>
                            <h6 class="category text-muted">
                                Chief Executive Officer
                            </h6>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 card-5">
                    <div class="card card-profile">
                        <div class="card-avatar">
                            <img class="img" src="{{asset('asset/images/avi.jpg')}}">
                        </div>
                        <div class="card-content">
                            <h4 class="card-title">
                                Avhishek Adhikari
                            </h4>
                            <h6 class="category text-muted">
                                Managing Director / PHP Developer
                            </h6>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 card-5">
                    <div class="card card-profile">
                        <div class="card-avatar">
                            <a href="https://www.linkedin.com/in/sushan-paudyal-375639107/" target="_blank">
                                <img class="img" src="{{asset('asset/images/sushan.jpg')}}">
                            </a>
                        </div>
                        <div class="card-content">
                            <h4 class="card-title">
                                <a href="https://www.linkedin.com/in/sushan-paudyal-375639107/" target="_blank"> Sushan Paudyal </a>
                            </h4>
                            <h6 class="category text-muted">
                                Software Developer
                            </h6>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 card-5">
                    <div class="card card-profile">
                        <div class="card-avatar">
                            <img class="img" src="{{asset('asset/images/deepak.jpg')}}">
                        </div>
                        <div class="card-content">
                            <h4 class="card-title">
                                Deepak Thapa Magar
                            </h4>
                            <h6 class="category text-muted">
                                Co-Founder / Marketing Director
                            </h6>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 card-5">
                    <div class="card card-profile">
                        <div class="card-avatar">
                            <img class="img" src="{{asset('asset/images/bishwo.jpg')}}">
                        </div>
                        <div class="card-content">
                            <h4 class="card-title">
                                Bishwo K.C.
                            </h4>
                            <h6 class="category text-muted">
                                WordPress Developer / FrontEnd Designer
                            </h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!--end blog section -->

    <div class="testimonials-2 section-white">
        <div class="container">
            <div class="row">
                <div class="carousel slide" data-ride="carousel" id="carousel-testimonial">
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <div class="card card-testimonial card-plain">
                                <div class="card-avatar">
                                    <img class="img" src="{{asset('asset/images/kendall.jpg')}}">
                                </div>
                                <div class="card-content">
                                    <h5 class="card-description">
                                        "I worked very well with DBASoftTech.They are great professional and knows the
                                        WordPress. Thank you for giving the best services."
                                        </br>
                                    </h5>
                                    <h4 class="card-title">
                                        Campan Campan
                                    </h4>
                                    <h6 class="category text-muted">
                                        Spain
                                    </h6>

                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="card card-testimonial card-plain">
                                <div class="card-avatar">
                                    <img class="img" src="{{asset('asset/images/bimal.jpg')}}">
                                </div>
                                <div class="card-content">
                                    <h5 class="card-description">
                                        "The services that they provide to us is awesome. They are responsive and
                                        deliver the services on time. Work is so much appreciate."
                                    </h5>
                                    <h4 class="card-title">
                                        Ktmimmigration
                                    </h4>
                                    <h6 class="category text-muted">
                                        Kathmandu, Nepal
                                    </h6>

                                </div>
                            </div>
                        </div>
                    </div>
                    <a class="left carousel-control" data-slide="prev" href="#carousel-testimonial" role="button">
                        <i aria-hidden="true" class="material-icons">
                            chevron_left
                        </i>
                    </a>
                    <a class="right carousel-control" data-slide="next" href="#carousel-testimonial" role="button">
                        <i aria-hidden="true" class="material-icons">
                            chevron_right
                        </i>
                    </a>
                </div>
            </div>
        </div>
    </div>



    <!--dbasofttech client -->
    <div class="client-dba">
        <div class="container-fluid">
            <div class="row row-centered">
                <h2 class="title">Our Client</h2>
                <div class="scroll-dba-client">
                    <div class="col-md-2 col-sm-2 col-centered">
                        <div class="card-image">
                            <a href="#">
                                <img class="img" src="{{asset('asset/images/ktmimmi.png')}}">
                            </a>
                            <div class="colored-shadow"
                                 style="background-image: url(asset/images/ktmimmi.png); opacity: 1;"></div>
                            <div class="ripple-container"></div>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-2 col-centered">
                        <div class="card-image">
                            <a href="#">
                                <img class="img" src="{{asset('asset/images/sumeru.png')}}">
                            </a>
                            <div class="colored-shadow"
                                 style="background-image: url(asset/images/sumeru.png); opacity: 1;"></div>
                            <div class="ripple-container"></div>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-2 col-centered">
                        <div class="card-image">
                            <a href="#">
                                <img class="img" src="{{asset('asset/images/damasco.jpg')}}">
                            </a>
                            <div class="colored-shadow"
                                 style="background-image: url(asset/images/damasco.jpg); opacity: 1;"></div>
                            <div class="ripple-container"></div>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-2 col-centered">
                        <div class="card-image">
                            <a href="#">
                                <img class="img" src="{{asset('asset/images/blackstone.png')}}">
                            </a>
                            <div class="colored-shadow"
                                 style="background-image: url(asset/images/blackstone.png); opacity: 1;"></div>
                            <div class="ripple-container"></div>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-2 col-centered">
                        <div class="card-image">
                            <a href="#">
                                <img class="img" src="{{asset('asset/images/dichm.png')}}">
                            </a>
                            <div class="colored-shadow"
                                 style="background-image: url(asset/images/dichm.png); opacity: 1;"></div>
                            <div class="ripple-container"></div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <!--footer section start -->
    <div id="contactsection" class="contactus-2">
        <div class="container">
            <div class="row">
                <div id="contactUs2Map" class="map"></div>
                <div class="col-md-6">
                    <div class="card card-contact card-raised">
                        <form role="form" id="contact-form" action="#" method="POST">
                            {{csrf_field()}}
                            <input type="hidden" name="_token" value="sjRWx6razZtbTtl81wJNIVx8M9wlcyICGLcIRiHM">
                            <div class="header header-raised header-rose text-center">
                                <h4 class="card-title">We help you to Transform Your Business</h4>
                            </div>
                            <div class="card-content">

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="info info-horizontal">
                                            <div class="icon icon-rose">
                                                <i class="material-icons">phone</i>
                                            </div>
                                            <div class="description">
                                                <h5 class="info-title">Give us a ring</h5>
                                                <p> Staff number<br>
                                                    +977 9860911876 <br>
                                                    +977 9841555156<br>
                                                    Sun - Fri, 10:00-6:00
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="info info-horizontal">
                                            <div class="icon icon-rose">
                                                <i class="material-icons">pin_drop</i>
                                            </div>
                                            <div class="description">
                                                <h5 class="info-title">Find us at the office</h5>
                                                <p> Chabail,Kathmandu<br>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group label-floating form-rose is-empty">
                                            <label class="control-label">Full Name</label>
                                            <input id="fname" type="text" name="fullname" class="form-control">
                                            <span class="material-input"></span></div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group label-floating form-rose is-empty">
                                            <label class="control-label">Email address</label>
                                            <input id="mail" type="email" name="email" class="form-control">
                                            <span class="material-input"></span></div>
                                    </div>
                                </div>

                                <div class="form-group label-floating form-rose is-empty">
                                    <label class="control-label">Your message</label>
                                    <textarea name="message" class="form-control" id="message" rows="6"></textarea>
                                    <span class="material-input"></span></div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="checkbox">
                                        </div>
                                    </div>
                                    <div class="col-md-6 down-up">
                                        <button type="submit" class="btn btn-primary pull-right down-down" disabled>Send Message
                                        </button>
                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="social-line social-line-white text-center">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="titles">Follow us on social media</h2>
                </div>
                <a href="https://twitter.com/DBASoftTech" class="btn btn-twitter btn-round" target="_blank">
                    <i class="fa fa-twitter"></i> Twitter · 2.5k
                </a>
                <a href="https://www.facebook.com/DBASOFTTECH09/" class="btn btn-facebook btn-round" target="_blank">
                    <i class="fa fa-facebook-square"></i> Facebook · 3.2k
                </a>
                <a href="#" class="btn btn-google btn-round">
                    <i class="fa fa-google-plus"></i> Google · 1.2k
                </a>
            </div>
        </div>
    </div>
    @endsection