@extends('admin.app')


@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <!-- top tiles -->
        <div class="row tile_count">
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-sticky-note"></i> Posts</span>
                <div class="count"> {{$post_count}}</div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-exclamation-circle"></i> Trashed Posts</span>
                <div class="count">{{ $trashed_count  }}</div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-user"></i> Users</span>
                <div class="count green">{{$user_count}}</div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-bookmark"></i> Tags</span>
                <div class="count">{{$tags_count}}</div>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
                <span class="count_top"><i class="fa fa-filter"></i> Categories </span>
                <div class="count">{{ $categories_count }}</div>
            </div>

        </div>
        <!-- /top tiles -->


        <br />





    </div>
    <!-- /page content -->


    @endsection