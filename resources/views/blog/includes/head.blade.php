<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<title>Blog Page</title>

<link rel="stylesheet" type="text/css" href="{{asset('asset/blog/app/css/fonts.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('asset/blog/app/css/crumina-fonts.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('asset/blog/app/css/normalize.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('asset/blog/app/css/grid.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('asset/blog/app/css/styles.css')}}">


<!--Plugins styles-->

<link rel="stylesheet" type="text/css" href="{{asset('asset/blog/app/css/jquery.mCustomScrollbar.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('asset/blog/app/css/swiper.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('asset/blog/app/css/primary-menu.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('asset/blog/app/css/magnific-popup.css')}}">

<link rel="stylesheet" href="{{asset('css/toastr.min.css')}}">

<!--Styles for RTL-->

<!--<link rel="stylesheet" type="text/css" href="app/css/rtl.css">-->

<!--External fonts-->

<link href='https://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
<style>
    .padded-50{
        padding: 40px;
    }
    .text-center{
        text-align: center;
    }
</style>