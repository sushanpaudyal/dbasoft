<!DOCTYPE html>
<html lang="en">
<head lang="en">
    @include('blog.includes.head')
</head>


<body class=" ">


@yield('title')
<div class="content-wrapper">

    @include('blog.includes.header')


    {{--<div class="header-spacer"></div>--}}

    @yield('main-section')




    <!-- Subscribe Form -->

    @include('blog.includes.subcriber')

    <!-- End Subscribe Form -->
</div>







<!-- Overlay Search -->

<div class="overlay_search">
    <div class="container">
        <div class="row">
            <div class="form_search-wrap">
                <form method="GET" action="/results">
                    <input class="overlay_search-input" placeholder="Type and hit Enter..." type="text" name="query">
                    <a href="#" class="overlay_search-close">
                        <span></span>
                        <span></span>
                    </a>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- End Overlay Search -->


@include('layouts.includes.footer')
@include('layouts.includes.head')
<!-- JS Script -->

<script src="{{asset('asset/blog/app/js/jquery-2.1.4.min.js')}}"></script>
<script src="{{asset('asset/blog/app/js/crum-mega-menu.js')}}"></script>
<script src="{{asset('asset/blog/app/js/swiper.jquery.min.js')}}"></script>
<script src="{{asset('asset/blog/app/js/theme-plugins.js')}}"></script>
<script src="{{asset('asset/blog/app/js/main.js')}}"></script>
<script src="{{asset('asset/blog/app/js/form-actions.js')}}"></script>
<script src="{{asset('js/toastr.min.js')}}"></script>
<script src="{{asset('asset/blog/app/js/velocity.min.js')}}"></script>
<script src="{{asset('asset/blog/app/js/ScrollMagic.min.js')}}"></script>
<script src="{{asset('asset/blog/app/js/animation.velocity.min.js')}}"></script>

<script>
    @if(Session::has('subscribed'))
        toastr.success('{{Session::get('subscribed')}}')
        @endif
</script>

{{--<!-- Go to www.addthis.com/dashboard to customize your tools -->--}}
{{--<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-59ae799e33749b95"></script>--}}
{{--<!-- ...end JS Script -->--}}
<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-59ae799e33749b95"></script>

</body>
</html>
