<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use App\Post;
use App\Tag;

class FrontEndController extends Controller
{
    public function index()
    {
        return view('blog.blog')->with('first_post', Post::orderBy('created_at', 'desc')->first())
            ->with('second_post', Post::orderBy('created_at','desc')->skip(1)->take(1)->get()->first())
            ->with('third_post', Post::orderBy('created_at','desc')->skip(2)->take(1)->get()->first())
            ->with('categories', Category::take(5)->get())
            ->with('laravel', Category::find(5))
            ->with('wordpress', Category::find(4))
            ->with('seo', Category::find(7));
    }

    public function singlePost($slug)
    {
        $post = Post::where('slug', $slug)->first();

        $next_id = Post::where('id', '>' , $post->id)->min('id');
        $prev_id = Post::where('id', '<', $post->id)->max('id');

        return view('blog.single')->with('post', $post)
            ->with('categories', Category::take(5)->get())
            ->with('next', Post::find($next_id))
            ->with('prev', Post::find($prev_id))
            ->with('tags', Tag::all())
            ->with('wordpress', Category::find(5));
    }

    public function category($id)
    {
        $category = Category::find($id);
        return view('blog.category')->with('category', $category)
            ->with('title', $category->name)
            ->with('categories', Category::take(5)->get());
        
    }
    public function tag($id)
    {
        $tag = Tag::find($id);
        return view('blog.tag')->with('tag', $tag)
            ->with('title', $tag->tag)
            ->with('categories', Category::take(5)->get())
            ->with('tags', Tag::all());
    }

}
