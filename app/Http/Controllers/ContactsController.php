<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ContactsController extends Controller
{
    public function store(Request $request)
    {
       $this->validate($request,[
           'fullname' => 'required',
           'email' => 'required',
           'message' => 'required'
       ]);
    }
}
