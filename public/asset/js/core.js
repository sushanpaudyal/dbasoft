/**
 * Created by woocodstudio on 5/10/2017.
 */
jQuery(document).ready(function () {
    jQuery("#contact").on('click', function () {
        jQuery('html, body').animate({
            'scrollTop': jQuery("#contactsection").position().top - 60
        });
    });

    jQuery("#serviceswe").on('click', function () {
        jQuery('html, body').animate({
            'scrollTop': jQuery("#servicesour").position().top - 60
        });
    });
});

jQuery(document).ready(function ($) {
    materialKitDemo.initContactUs2Map();
});

jQuery(document).ready(function () {
    jQuery('.scroll-dba-client').slick({
        dots: false,
        autoplay: true,
        infinite: true,
        speed: 300,
        slidesToShow: 5,
        slidesToScroll: 1,
        prevArrow: false,
        nextArrow: false,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 6,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: false,
                    prevArrow: false,
                    nextArrow: false
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 3,
                    infinite: true,
                    slidesToScroll: 1,
                    prevArrow: false,
                    nextArrow: false
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    infinite: true,
                    slidesToScroll: 1,
                    prevArrow: false,
                    nextArrow: false
                }
            }
        ]
    });
});

