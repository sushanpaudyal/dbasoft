<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('dba');

Auth::routes();

Route::get('/blog', 'FrontEndController@index')->name('blog.index');
Route::get('/blog/{slug}', 'FrontEndController@singlePost')->name('post.single');
Route::get('/category/{id}', 'FrontEndController@category')->name('category.single');
Route::get('/tag/{id}', 'FrontEndController@tag')->name('tag.single');

Route::get('/results', function (){
      $posts = \App\Post::where('title', 'like', '%' . request('query') . '%')->get();
      return view('blog.results')->with('posts', $posts)
          ->with('title', 'Search results: ' . request('query'))
          ->with('categories', \App\Category::take(5)->get())
          ->with('query', request('query'));
});

Route::post('/subscribe', function(){
    $email = request('email');
    Newsletter::subscribe($email);
    Session::flash('subscribed', 'Successfully Subcribed');
    return redirect()->back();
});

Route::post('/contact/store', 'ContactsController@store')->name('contact.store');


Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function ()
{
    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('/post/create', 'PostController@create')->name('post.create');
    Route::post('/post/store', 'PostController@store')->name('post.store');
    Route::get('/posts','PostController@index')->name('posts');
    Route::get('/post/delete/{id}', 'PostController@destroy')->name('post.delete');
    Route::get('/post/kill/{id}', 'PostController@kill')->name('post.kill');
    Route::get('/posts/edit/{id}', 'PostController@edit')->name('posts.edit');
    Route::post('/posts/update/{id}', 'PostController@update')->name('posts.update');
    Route::get('/post/restore/{id}', 'PostController@restore')->name('post.restore');

    Route::get('/post/trashed', 'PostController@trashed')->name('post.trashed');

    Route::get('category/create', 'CategoryController@create')->name('category.create');
    Route::post('category/store', 'CategoryController@store')->name('category.store');
    Route::get('/categories', 'CategoryController@index')->name('categories');
    Route::get('/category/edit/{id}', 'CategoryController@edit')->name('category.edit');
    Route::get('/category/delete/{id}', 'CategoryController@destroy')->name('category.delete');
    Route::post('/category/update/{id}', 'CategoryController@update')->name('category.update');



    Route::get('/tags', 'TagsController@index')->name('tags');
    Route::get('/tag/edit/{id}', 'TagsController@edit')->name('tag.edit');
    Route::post('/tag/update/{id}', 'TagsController@update')->name('tag.update');
    Route::get('/tag/delete/{id}', 'TagsController@destroy')->name('tag.delete');
    Route::get('/tag/create', 'TagsController@create')->name('tag.create');
    Route::post('/tag/store', 'TagsController@store')->name('tag.store');


    Route::get('/users', 'UsersController@index')->name('users');
    Route::get('/user/create', 'UsersController@create')->name('user.create');
    Route::post('/user/store', 'UsersController@store')->name('user.store');
    Route::get('user/delete/{id}', 'UsersController@destroy')->name('user.delete');

    Route::get('user/admin/{id}', 'UsersController@admin')->name('user.admin');
    Route::get('user/notadmin/{id}', 'UsersController@notadmin')->name('user.notadmin');


    Route::get('users/profile', 'ProfilesController@index')->name('user.profile');
    Route::post('users/profile/update', 'ProfilesController@update')->name('user.profile.update');


});


