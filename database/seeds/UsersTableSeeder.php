<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Profile;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = App\User::create([
            'name' => 'DbaSoftTech',
            'email' => 'info@dbasofttech.com',
            'password' => bcrypt('P@ssw0rd'),
            'admin' => 1
        ]);

        App\Profile::create([
          'user_id' => $user->id,
            'avatar' => 'uploads/avatars/avatar.png',
            'about' => 'DBASoftTech is an IT company empowering and trying to empower every sector with 
                      new tools and technologies in information technology. We are a group of IT Professionals driven towards 
                     implementing the best of technology and providing solution for various 
                      disciplines with the help of information technology.Our dedicated consistently delivers superior quality products and services',
            'facebook' => 'https://www.facebook.com/profile.php?id=1408528815837839',
            'linkedin' => 'https://www.linkedin.com/company/13312905/'
        ]);
    }
}
